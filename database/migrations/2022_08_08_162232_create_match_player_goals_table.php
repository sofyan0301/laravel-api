<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchPlayerGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_player_goals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('match_schedule_id')->unsigned();
            $table->integer('player_id')->unsigned();
            $table->time('time_of_goal');
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('match_schedule_id')->references('id')->on('match_schedules')->onDelete('cascade');
            $table->foreign('player_id')->references('id')->on('players')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_player_goals');
    }
}
