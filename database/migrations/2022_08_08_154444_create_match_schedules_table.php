<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date_match');
            $table->time('time_match');
            $table->integer('team_home_id')->unsigned();
            $table->integer('team_away_id')->unsigned();
            $table->string('score')->nullable();
            $table->integer('winning_team_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('team_home_id')->references('id')->on('teams')->onDelete('cascade');
            $table->foreign('team_away_id')->references('id')->on('teams')->onDelete('cascade');
            $table->foreign('winning_team_id')->references('id')->on('teams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_schedules');
    }
}
