<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MatchSchedule extends Model
{
    use SoftDeletes;

    protected $table = 'match_schedules';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date_match', 'time_match', 'team_home_id', 'team_away_id', 'score', 'winning_team_id'
    ];

    public function playerGoals() {
        return $this->hasMany(MatchPlayerGoal::class);
    }

    public function playerNameGoals() {
        $data = MatchPlayerGoal::selectRaw('players.name as name, count(match_player_goals.player_id) AS `count`')
        ->where('match_schedule_id', $this->id)
        ->join('players', 'players.id', '=', 'match_player_goals.player_id')
        ->groupBy('player_id')
        ->orderBy('count','DESC')
        ->first();
        return $data->name;
    }

    public function getAccumulation($team_id) {
        $total_win = MatchSchedule::where('winning_team_id', '=', $team_id)->count();
        return $total_win;
    }

    public function teamHome()
    {
        return $this->hasOne(Teams::class, 'id', 'team_home_id');
    }

    public function teamAway()
    {
        return $this->hasOne(Teams::class, 'id', 'team_away_id');
    }
}
