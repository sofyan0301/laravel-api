<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MatchPlayerGoal extends Model
{
    use SoftDeletes;

    protected $table = 'match_player_goals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'match_schedule_id', 'player_id', 'time_of_goal'
    ];

    public function player()
    {
        return $this->hasOne(Player::class, 'id', 'player_id');
    }

    public function matchSchedule()
    {
        return $this->hasOne(MatchSchedule::class, 'id', 'match_schedule_id');
    }
}
