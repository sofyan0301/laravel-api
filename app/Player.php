<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Player extends Model
{
    use SoftDeletes;

    protected $table = 'players';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'team_id', 'height', 'weight', 'position', 'back_number'
    ];

    public function team()
    {
        return $this->hasOne(Teams::class, 'id', 'team_id');
    }
}
