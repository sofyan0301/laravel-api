<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MatchSchedule;
use App\MatchPlayerGoal;

class ReportMatchScheduleController extends Controller
{
    public function __construct(MatchSchedule $matchSchedule, MatchPlayerGoal $matchPlayerGoal)
    {
        $this->matchSchedule = $matchSchedule;
        $this->matchPlayerGoal = $matchPlayerGoal;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = $this->matchSchedule->paginate(10);
        
        $data_response = $model
            ->getCollection()
            ->map(function($item) {
                return [
                    'id' => $item->id,
                    'date_match' => $item->date_match,
                    'time_match' => $item->time_match,
                    'team_home' => $item->teamHome->team_name,
                    'team_away' => $item->teamAway->team_name,
                    'score' => $item->score,
                    'status' => $item->winning_team_id === $item->team_home_id ? 'Tim Home Menang' : ($item->winning_team_id === $item->team_away_id ? 'Tim Away Menang' : 'Draw'),
                    'most_goals' => $item->playerNameGoals(),
                    'home_accumulation' => $item->getAccumulation($item->team_home_id),
                    'away_accumulation' => $item->getAccumulation($item->team_away_id)

                ];
        })->toArray();
        return response()->json([
            'status' => true,
            'data' => $data_response,
            'message' => 'sueccess'
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
