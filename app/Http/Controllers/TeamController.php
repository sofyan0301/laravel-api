<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teams;

class TeamController extends Controller
{
    public function __construct(Teams $team)
    {
        $this->team = $team;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'status' => true,
            'data' => $this->team->paginate(10),
            'message' => 'sueccess'
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            'team_name' => 'required',
            'logo' => 'required',
            'since' => 'required|integer',
            'address' => 'required',
            'city' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validate->messages(),
                'data' => null
            ]);
        }
        $data_post = $request->all();
        $filename = $this->saveImage($request);
        if ($filename !== '') {
            $data_post['logo'] = $filename;
        }
        
        $model = $this->team->create($data_post);
        if ($model) {
            return response()->json([
                'error' => false,
                'message' => "Create team successfully.",
                'data' => $model
            ]);
        }

        return response()->json([
            'error' => true,
            'message' => "Internal server error",
            'data' => null
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = \Validator::make($request->all(), [
            'team_name' => 'required',
            'logo' => 'required',
            'since' => 'required|integer',
            'address' => 'required',
            'city' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validate->messages(),
                'data' => null
            ]);
        }

        $data_post = $request->all();
        $filename = $this->saveImage($request);
        if ($filename !== '') {
            $data_post['logo'] = $filename;
        }

        $model = $this->team->find($id)->update($data_post);
        if ($model) {
            return response()->json([
                'error' => false,
                'message' => "Updated team successfully.",
                'data' => $model
            ]);
        }

        return response()->json([
            'error' => true,
            'message' => "Internal server error",
            'data' => null
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->team->findOrFail($id);
        if (!$model) {
            return response()->json([
                'error' => true,
                'message' => "Data not found.",
                'data' => null
            ]);
        } else {
            if ($model->destroy($id)) {
                return response()->json([
                    'error' => false,
                    'message' => "Data successfully deleted.",
                    'data' => null
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => "Internal server error.",
                    'data' => null
                ]);
            }
        }
    }

    private function saveImage($request)
    {
        $data = $request->get('logo');
        $imageurl = storage_path('app/public/');
        $filename = '';
        if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $data = base64_decode($data);
            $type = strtolower($type[1]);
            if (!$filename) {
                $filename = md5(uniqid()) . '.' . $type;
                file_put_contents($imageurl . $filename, $data);
            }
        }

        return $filename;
    }
}
