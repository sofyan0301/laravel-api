<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MatchSchedule;
use App\MatchPlayerGoal;

class MatchScheduleController extends Controller
{
    public function __construct(MatchSchedule $matchSchedule, MatchPlayerGoal $matchPlayerGoal)
    {
        $this->matchSchedule = $matchSchedule;
        $this->matchPlayerGoal = $matchPlayerGoal;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'status' => true,
            'data' => $this->matchSchedule->with('playerGoals')->paginate(10),
            'message' => 'sueccess'
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            'date_match' => 'required',
            'time_match' => 'required',
            'team_home_id' => 'required|different:team_away_id',
            'team_away_id' => 'required|different:team_home_id'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validate->messages(),
                'data' => null
            ]);
        }
        
        $model = $this->matchSchedule->create($request->all());
        if ($model) {
            return response()->json([
                'error' => false,
                'message' => "Create data successfully.",
                'data' => $model
            ]);
        }

        return response()->json([
            'error' => true,
            'message' => "Internal server error",
            'data' => null
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = \Validator::make($request->all(), [
            'score' => 'required',
            'winning_team_id' => 'required',
            'player_goals.*.player_id' => 'required',
            'player_goals.*.time_of_goal' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validate->messages(),
                'data' => null
            ]);
        }

        $player_goals = $request->get('player_goals');
        if (count($player_goals)) {
            $this->matchPlayerGoal->where('match_schedule_id', $id)->delete();
            foreach ($player_goals as $key => $player_goal) {
                $data_post = [
                    'match_schedule_id' => $id,
                    'player_id' => $player_goal['player_id'],
                    'time_of_goal' => $player_goal['time_of_goal']
                ];
                $this->matchPlayerGoal->create($data_post);
            }
        }

        $model = $this->matchSchedule->find($id)->update($request->all());
        if ($model) {
            return response()->json([
                'error' => false,
                'message' => "Updated successfully.",
                'data' => $model
            ]);
        }

        return response()->json([
            'error' => true,
            'message' => "Internal server error",
            'data' => null
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
