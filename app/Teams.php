<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teams extends Model
{
    use SoftDeletes;

    protected $table = 'teams';
    protected $appends = array('logo_url');
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_name', 'logo', 'since', 'address', 'city'
    ];

    public function getPlayes()
    {
        return $this->hasMany(Player::class);
    }

    public function getLogoUrlAttribute()
    {
        return url('/api/media') . '/' . $this->logo;
    }
}
