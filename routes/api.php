<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// route of teams
Route::resource('/teams', 'TeamController');

// route of match players 
Route::resource('/players', 'PlayersController', ['only' => ['index','store']]);

// route of match schedule 
Route::resource('/schedules', 'MatchScheduleController', ['only' => ['index','store', 'update']]);

// route of report match schedule 
Route::resource('/report-schedules', 'ReportMatchScheduleController', ['only' => ['index']]);

Route::get('media/{filename}', function ($filename) {
    $path = storage_path('app/public/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});